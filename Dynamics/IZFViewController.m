//
//  IZFViewController.m
//  Dynamics
//
//  Created by Izzy Fraimow on 11/13/13.
//  Copyright (c) 2013 Izzy Fraimow. All rights reserved.
//

#import "IZFViewController.h"

@import AudioToolbox;

@interface IZFViewController () <UICollisionBehaviorDelegate, UIDynamicAnimatorDelegate>

@property (weak, nonatomic) IBOutlet UIView *shade;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstraint;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *panGestureRecognizer;

@property (nonatomic, strong) UIDynamicAnimator *animator;

@property (nonatomic, strong) UIAttachmentBehavior *attachBehavior;
@property (nonatomic, strong) UIDynamicItemBehavior *dynamicItemBehavior;
@property (nonatomic, strong) UIDynamicBehavior *timedBehavior;

@property (nonatomic, assign) NSTimeInterval animationDuration;
@property (nonatomic, assign) CGPoint finalVelocity;

@property (nonatomic, assign) BOOL dragging;

@end

@implementation IZFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.panGestureRecognizer.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated; {
//    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.shade
//                                                              attribute:NSLayoutAttributeHeight
//                                                              relatedBy:NSLayoutRelationEqual
//                                                                 toItem:self.view
//                                                              attribute:NSLayoutAttributeHeight
//                                                             multiplier:1
//                                                               constant:0];
//    [self.view addConstraint:height];
//    
//    self.topConstraint.constant = 0;
//    self.leftConstraint.constant = 0;
//    self.rightConstraint.constant = 0;
}

- (void)viewDidAppear:(BOOL)animated; {
    [super viewDidAppear:animated];
    
//    UIGravityBehavior *gravityBehavior = [self step1];
//    UICollisionBehavior *collisionBehavior = [self step2];
//    UIAttachmentBehavior *attachmentBehavior = [self step3];
//    self.attachBehavior = attachmentBehavior;
//
//    //step 4 is part of -pan
//    UIDynamicItemBehavior *elasticityBehavior = [self step5];
//    
//    [self.animator addBehavior:gravityBehavior];
//    [self.animator addBehavior:collisionBehavior];
//    [self.animator addBehavior:elasticityBehavior];
    
//    [self step6];
//    [self step7];
//    [self step8];
    
}

- (IBAction)viewTapped:(id)sender {
//    UIGravityBehavior *gravityBehavior = [self step1];
//    [self.animator addBehavior:gravityBehavior];
	
//    UICollisionBehavior *collisionBehavior = [self step2];
//    [self.animator addBehavior:collisionBehavior];
	
//    [self step4:5];
}

- (UIGravityBehavior *)step1; {
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.shade]];
    return gravityBehavior;
}

- (UICollisionBehavior *)step2; {
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[self.shade]];
    [collisionBehavior setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(-CGRectGetHeight(self.view.bounds), 0, 0, 0)];
    collisionBehavior.collisionDelegate = self;
    return collisionBehavior;
}

- (UIAttachmentBehavior *)step3; {
    UIAttachmentBehavior *attachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem:self.shade
                                                                         attachedToAnchor:self.shade.center];

    attachmentBehavior.damping = 1.0;
    
    self.panGestureRecognizer.enabled = YES;
    
    return attachmentBehavior;
}

- (void)step4:(CGFloat)velocity; {
    UIPushBehavior *push = [[UIPushBehavior alloc] initWithItems:@[self.shade]
                                                            mode:UIPushBehaviorModeInstantaneous];
    push.pushDirection = CGVectorMake(0, -1);
    push.magnitude = velocity * 20;
    [self.animator addBehavior:push];
    //[self.timedBehavior addChildBehavior:push];
}

- (UIDynamicItemBehavior *)step5; {
    UIDynamicItemBehavior *elasticityBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.shade]];
    elasticityBehavior.elasticity = 0.4f;
    elasticityBehavior.allowsRotation = NO;
//    elasticityBehavior.density = 0.2;
    
    self.dynamicItemBehavior = elasticityBehavior;
    
    return elasticityBehavior;
}

- (void)step6; {
    [self.animator addBehavior:self.timedBehavior];
}

- (void)step7; {
    self.animator.delegate = self;
}

- (void)step8; {
    __weak typeof(self) weakSelf = self;
    _timedBehavior.action = ^{
        if(weakSelf.animator.elapsedTime < weakSelf.animationDuration) {
            NSLog(@"-> %f -> %f", weakSelf.animator.elapsedTime, weakSelf.animationDuration);
            return;
        }
        
        weakSelf.finalVelocity = [weakSelf.dynamicItemBehavior linearVelocityForItem:weakSelf.shade];
        [weakSelf.animator removeAllBehaviors];
    };
}

- (UIDynamicBehavior *)timedBehavior; {
    if(_timedBehavior) {
        return _timedBehavior;
    }
    
    UIGravityBehavior *gravityBehavior = [self step1];
    UICollisionBehavior *collisionBehavior = [self step2];
    UIAttachmentBehavior *attachmentBehavior = [self step3];
    self.attachBehavior = attachmentBehavior;
    
    UIDynamicItemBehavior *elasticityBehavior = [self step5];
    
    UIDynamicBehavior *timedBehavior = [[UIDynamicBehavior alloc] init];
    [timedBehavior addChildBehavior:gravityBehavior];
    [timedBehavior addChildBehavior:collisionBehavior];
    [timedBehavior addChildBehavior:elasticityBehavior];
    
    _timedBehavior = timedBehavior;
    
    self.animationDuration = 0.25;
    
    //[self step8];

    return _timedBehavior;
}

- (IBAction)pan:(UIPanGestureRecognizer *)pan {
	{
    if(![self.animator.behaviors count]) {
        [self.animator addBehavior:self.timedBehavior];
    }
    
    if(self.animator.running) {
        self.animationDuration = self.animator.elapsedTime + 0.25;
    }
	}
    
    CGPoint point = [pan locationInView:self.view];
    CGPoint attachmentPoint = self.attachBehavior.anchorPoint;
    attachmentPoint.y = point.y;
	self.attachBehavior.anchorPoint = attachmentPoint;
    
    switch (pan.state) {
        case UIGestureRecognizerStatePossible:
            break;
        case UIGestureRecognizerStateBegan: {
            self.dragging = YES;
            //[self.animator addBehavior:self.attachBehavior];
            [self.timedBehavior addChildBehavior:self.attachBehavior];
        }
        case UIGestureRecognizerStateChanged: {
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
            self.dragging = NO;
            CGFloat veloicity = [pan velocityInView:self.view].y;
            [self step4:(-veloicity/CGRectGetHeight(self.view.bounds))];
            [self.animator removeBehavior:self.attachBehavior];
            //[self.timedBehavior removeChildBehavior:self.attachBehavior];
            break;
        }
        case UIGestureRecognizerStateFailed:
            break;
    }
}

- (void)collisionBehavior:(UICollisionBehavior*)behavior
      beganContactForItem:(id <UIDynamicItem>)item
   withBoundaryIdentifier:(id <NSCopying>)identifier
                  atPoint:(CGPoint)p; {
    if(p.y >= 0) {
        return;
    }
    
    [self.animator removeAllBehaviors];

//    [self.view setNeedsLayout];
//    [self.view layoutIfNeeded];
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator; {
    if(self.dragging) {
        return;
    }
    [self.animator removeAllBehaviors];
    self.timedBehavior = nil;
    
    CGRect shadeFrame = self.shade.frame;
    
    BOOL opening = self.finalVelocity.y > 0;
    
    CGFloat maxDistance = CGRectGetHeight(self.view.bounds);
    CGFloat travelDistance;
    if(opening) {
        travelDistance = maxDistance - ABS(shadeFrame.origin.y);
        self.topConstraint.constant = -shadeFrame.size.height;
    } else {
        travelDistance = ABS(shadeFrame.origin.y);
        self.topConstraint.constant = 0;
    }
    CGFloat distanceRatio = travelDistance / maxDistance;
    
    [self.view setNeedsLayout];
    NSTimeInterval duration = distanceRatio * 0.75;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

@end
