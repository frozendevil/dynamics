//
//  main.m
//  Dynamics
//
//  Created by Izzy Fraimow on 11/13/13.
//  Copyright (c) 2013 Izzy Fraimow. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IZFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IZFAppDelegate class]));
    }
}
