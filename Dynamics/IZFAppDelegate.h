//
//  IZFAppDelegate.h
//  Dynamics
//
//  Created by Izzy Fraimow on 11/13/13.
//  Copyright (c) 2013 Izzy Fraimow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IZFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
